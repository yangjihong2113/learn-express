const express = require('express');
const path = require('path')
const cors = require('cors')
const app = express();
app.use(cors())

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});
 
app.get('/info', function(req, res){
    res.send({
        text: 'success'
    })
})
app.listen(3000);
