const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')
//const cors = require('cors')
const app = express();
//app.use(cors())

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});


app.get('/info', function (req, res) {
    res.end('ok')
});

// 使用中间件自定义响应头
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
    res.setHeader('Access-Control-Allow-Methods', 'PUT')
    // 使用响应头这个允许其他类型的content-type
    res.setHeader('Access-Control-Allow-Headers', 'token, Content-Type') 
    // 允许跨域携带 cookie
    res.setHeader('Access-Control-Allow-Credentials', 'true') 
    next()
})

app.put('/info', function (req, res) {
    res.end('ok')
});
  
app.listen(3000);
