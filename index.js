const express = require('express')
const path = require('path')

const app = express()

app.get('/', function (req, res) {
  res.sendFile(path.resolve('index.html'), )
})


app.get('/userInfo', function (req, res) {
  res.status(200).end(JSON.stringify({
    uid: 1,
    name: 'test'
  }))
})

app.engine('.html', require('ejs').__express);
// 设置默认路径
app.set('views', path.join(path.resolve()));
// 设置引擎文件后缀名是 .html
app.set('view engine', 'html');

app.get('/template', function (req, res) {
  res.render('template', {name: '你好你好'})
})

app.listen(3000)