// 使用子进程执行命令
const { exec } = require('child_process')

// 获取运行命令的参数
// 假设运行 npm run dev ejs ，那么就得到 process.argv[2] === ejs
// 为什么是第2个参数，因为在package.json中已经有两个了，
// 我们可以打印出来看看
//console.log('node 参数', process.argv)

// 获取当前运行的服务
const funDir = process.argv[2] || '.'

// 实际执行的命令
const command = `node ${funDir}/index.js`
// child_process 是 node 的子进程， exec 可以执行node 命令x
const p = exec(command)
// 将子进程的标准输出直接传输到当前进程的标准输出，这样，当子进程产生输出时，它将被打印到当前进程的控制台上。
p.stdout.pipe(process.stdout)

