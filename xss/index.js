const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')
const fs = require('fs')

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// ———————————存储型 xss 攻击—————————————

// 首页 -> 用户1 保存数据
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

// 首页 -> 保存书名
app.post('/saveName', function (req, res) {
  const { name } = req.body
  fs.writeFileSync(path.resolve(__dirname, 'data.txt'), name)
  res.send('保存成功');
});


// 新建另一个页面 
app.get('/index2', function (req, res) {
  res.sendFile(path.join(__dirname, '/index2.html'));
});
// 另一个页面 -> 用户2 获取数据
app.get('/getName', function (req, res) {
  // 从 data.txt 中取出存储的书名
  const name = fs.readFileSync(path.resolve(__dirname, 'data.txt')).toString()
  console.log(name)
  res.send(
    JSON.stringify({
      name: name,
    })
  );
});

// ———————————反射型 xss 攻击—————————————

app.get('/index3', function(req, res) {
  res.sendFile(path.join(__dirname, '/index3.html'));
})

app.get('/reflect', function(req, res) {
  const xss = req.query.xss
  res.send(xss)
})

// ——————————————DOM型 xss 攻击——————————————————————

app.get('/index4', function(req, res) {
  res.sendFile(path.join(__dirname, '/index4.html'));
})


app.listen(3000);
