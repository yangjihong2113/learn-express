const express = require('express');
const path = require('path')
const app = express();

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});
  
app.get('/data', function (req, res) {
    const { query } = req
    // 获取参数上的回调
    const callback = query.callback
    // 服务端返回执行回调函数，并传递参数 { name: 'test' }
    res.send(`${callback}({ name: 'test' })`)
});
    
app.listen(3000);
