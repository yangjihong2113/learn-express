const express = require('express');
const path = require('path');

const app = express();
// 先禁用协商缓存的头部
app.disable('etag')

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'index.html'));
});

app.get('/info', (req, res) => {
  try {
    // 设置一分钟后过期
    const now = new Date();
    // 计算时差
    const now2 = now.setTime(
      now.getTime() - now.getTimezoneOffset() * 60 + 1 * 60
    );
    // 转换成 GMT 时间
    const expiresDate = new Date(now2).toUTCString()
    // 设置响应头
    res.setHeader('expires', expiresDate);
    res.setHeader('cache-control', 'max-age=5');
    res.send('ok');
  } catch (err) {
    console.log(err);
  }
});

// cache-control 响应头
app.get('/name', (req, res) => {
  try {
    //res.setHeader('cache-control', 'no-store');
    //res.setHeader('cache-control', 'no-cache');
    //res.setHeader('cache-control', 'max-age=10');
    res.send('ok');
  } catch(err) {
    console.log(err)
  }

})

// 协商缓存
let lastModifed = ''
app.get('/negotiate', (req, res) => {
  try {
    const ifModifiedSince = req.get('if-modified-since')
    if (ifModifiedSince === lastModifed) {
      // 手动设置 304 请求状态码
      res.status(304)
      res.end('ok')
      return
    }
    lastModifed = new Date().toUTCString()
    // 设置 GMT 时间
    res.setHeader('last-modified', lastModifed)
    res.end('ok')
  } catch(err) {
    console.log(err)
  }
})

// etag/if-none-match 协商缓存
let etag = ''
app.get('/negotiate1', (req, res) => {
  try {
    const ifNoneMatch = req.get('if-none-match')
    if (ifNoneMatch === etag) {
      // 手动设置 304 请求状态码
      res.status(304)
      res.end('ok')
      return
    }
    etag = 'etag value'
    // 设置 GMT 时间
    res.setHeader('etag', etag)
    res.end('ok')
  } catch(err) {
    console.log(err)
  }
})

// 强缓存+协商缓存
app.get('/allCache', (req, res) => {
  try {

    const ifNoneMatch = req.get('if-none-match')
    if (ifNoneMatch === etag) {
      // 手动设置 304 请求状态码
      res.status(304)
      res.end('ok')
      return
    } 

    res.setHeader('cache-control', 'max-age=10')
    etag = 'etag value'
    // 设置 GMT 时间
    res.setHeader('etag', etag)
    res.end('ok')
  } catch(err) {
    console.log(err)
  }
})


app.listen(3000);
