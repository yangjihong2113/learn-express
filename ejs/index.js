const express = require('express')
const path = require('path')

const app = express()

app.engine('.html', require('ejs').__express);
// 设置默认路径 在 ejs 文件夹下
app.set('views', path.join(path.resolve(), 'ejs'));
// 设置引擎文件后缀名是 .html
app.set('view engine', 'html');

// 访问路径 改成 /ejs
app.get('/ejs', function (req, res) {
  res.render('template', {ejsName: 'ejs 你好你好'})
})

app.listen(3000)