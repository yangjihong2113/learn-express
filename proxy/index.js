const express = require('express');
const path = require('path')
const { createProxyMiddleware } = require('http-proxy-middleware');
const app = express();

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});
 
// 把针对 /api 的请求，转发给 3001 端口的服务为
app.use('/api', createProxyMiddleware({
    target: 'http://localhost:3001',
    timeout: 3000,
    changeOrigin: true,
}))

app.listen(3000);

// 第二个服务 3001 端口，未配置允许跨域请求
const app1 = express();
app1.get('/api/info', function(req, res){
    res.send('proxy ok')
})
app1.listen(3001)
