const express = require('express');
const path = require('path');
const cors = require('cors');
const cookieParser = require('cookie-parser');

// 第一个服务
const app = express();
// 允许跨域请求
app.use(cors());
// 解析 cookie
app.use(cookieParser());

// 安全的网页，用于用户登录
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

// 安全的网页的登录接口，给网站设置 cookie
app.get('/getInfo', function (req, res) {
  res.cookie('userId', '111111', {
    expires: new Date(Date.now() + 900000),
    sameSite: 'Strict', // 设置为 Strict 任何情况都不懈怠第三封 cookie，也可以设置为 Lax
    secure: 'true', //只要是 truthy 值就可以
    path: '/',
  });
  res.send('end');
});
// 修改密码的接口
app.get('/savePwd', function (req, res, next) {
  const { query, cookies } = req;
  // 能获取 cookie 才返回修改成功
  // 使用 req.get 获取请求头的字段
  const referer = req.get('referer')
  if (referer === 'https://www.danger.com/') {
    res.json({
      pwd: query.pwd,
      message: '修改失败',
    });
  }

  // cookie 双重验证
  if (cookies.userId && query.userId === cookies.userId) {
    res.json({
      pwd: query.pwd,
      message: '修改成功',
    });
  } else {
    res.json({
      pwd: query.pwd,
      message: '修改失败',
    });
  }
});
app.listen(3000)

// 第二个服务
// 不安全的网页，用于用 iframe/ a 标签 等 嵌入安全的网页，盗用 cookie
const app2 = express();
app2.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '/index2.html'));
  });
app2.listen(3001)


